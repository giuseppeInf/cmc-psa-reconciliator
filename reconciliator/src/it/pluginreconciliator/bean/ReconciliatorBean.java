package it.pluginreconciliator.bean;

public class ReconciliatorBean {
	private String codiceConc;
	private String numFattura;
	private String client;
	
	public ReconciliatorBean(String codiceConc, String numFattura, String client) {
		this.codiceConc = codiceConc;
		this.numFattura = numFattura;
		this.client = client;
	}

	public String getCodiceConc() {
		return codiceConc;
	}

	public void setCodiceConc(String codiceConc) {
		this.codiceConc = codiceConc;
	}

	public String getNumFattura() {
		return numFattura;
	}

	public void setNumFattura(String numFattura) {
		this.numFattura = numFattura;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}	
}
