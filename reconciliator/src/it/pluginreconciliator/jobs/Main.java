package it.pluginreconciliator.jobs;


import java.util.Properties;
import java.util.Vector;
import javax.swing.JOptionPane;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import com.google.gson.Gson;
import it.pluginreconciliator.rest.RestClient;
import it.pluginreconciliator.json.OperationBuilder;
import it.pluginreconciliator.bean.ReconciliatorBean;
import it.pluginreconciliator.commons.Constants;
import it.pluginreconciliator.excelreader.ExcelReader;
import it.pluginreconciliator.utility.Utility;

public class Main {

	public static void main(String[] args) throws EncryptedDocumentException, InvalidFormatException {

		try {
			Properties prop = Utility.loadProperties();

			String path = "C:\\Users\\Giuseppe\\Desktop\\TEST2.xlsx";

			Vector<ReconciliatorBean> listOfBean;

			ExcelReader reader = new ExcelReader(path);

			listOfBean = reader.readFile();

			if (listOfBean == null) {
				JOptionPane.showMessageDialog(null, "Nessuna riga trovata!");
				return;
			}
		
			RestClient restClient = new RestClient();
			
			OperationBuilder op = new OperationBuilder(prop.getProperty("repo-username"), prop.getProperty("repo-password"));
						
			Gson gson = new Gson();
			String json = gson.toJson(op);
									
			String authToken = restClient.doPost(prop.getProperty("repo-url") + prop.getProperty("repo-login"), json);
			authToken = authToken.replace("\"", "");

					
			for (int i = 0; i < listOfBean.size(); i++) {
				if (listOfBean.elementAt(i) != null) {
					String codiceConc = listOfBean.elementAt(i).getCodiceConc();
					String numFattura = listOfBean.elementAt(i).getNumFattura();
					String client = listOfBean.elementAt(i).getClient();

					if (Utility.checkDocNumber(numFattura) && Utility.checkBrand(client)
							&& Utility.checkDealerCode(codiceConc)) {
						if (Constants.AC.equalsIgnoreCase(client) || Constants.DS.equalsIgnoreCase(client))
							codiceConc = "2".concat(codiceConc);
						else if (Constants.AP.equalsIgnoreCase(client))
							codiceConc = "1".concat(codiceConc);
						else {
							Utility.logger.error("Il codice concessionario � errato!");
							break;
						}

						int clientToInt = 0;

						if (codiceConc.startsWith("2"))
							clientToInt = 1;
						else if (codiceConc.startsWith("1"))
							clientToInt = 2;
						
						if(clientToInt != 0) { 
							String response = restClient.doGet(prop.getProperty("repomanger-ws") + codiceConc + "/" + numFattura + "/" + clientToInt, null, authToken);
							if(response == "1") {
								Utility.logger.error("There are not operations sent to infocert");
							}
							else if(response == "2") {
								Utility.logger.error("The client received is not correct");
							}
						} else {
							Utility.logger.error("Il brand � sbagliato!"); 
						}   

					} else {
						Utility.logger.error(
								"Qualche informazione non � stata calcolata correttamente oppure � errata nel file originale.");
						Utility.logger.error("Ecco le informazione ricavate:");
						Utility.logger.error("Codice concessionario ricavato: " + codiceConc);
						Utility.logger.error("Numero fattura: " + numFattura);
						Utility.logger.error("Brand: " + client);
					}
				}
			}
			
	/*		for (int i = 0; i < listOfBean.size(); i++) {
				System.out.println(listOfBean.elementAt(i).getCodiceConc() + " " + listOfBean.elementAt(i).getNumFattura() + " " + listOfBean.elementAt(i).getClient());
			}   */
		} catch (Exception ex) {
			ex.printStackTrace();
			Utility.logger.error("Generic error (Error in the call to web service).");
		}
	}
}
