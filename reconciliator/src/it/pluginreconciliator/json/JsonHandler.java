package it.pluginreconciliator.json;

import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import it.pluginreconciliator.utility.Utility;

public class JsonHandler {
	
	// Build a json string representing the object passed as parameters
	public String serialize(Object object) throws Exception {
		try {
			GensonBuilder builder = new GensonBuilder();
			builder.setSkipNull(true); // if true, null value are not included into json
			Genson instance = builder.create();
			String json = instance.serialize(object);
			Utility.logger.debug(json);
			return json;
		} catch(Exception e) {
			Utility.logger.error(e);
			throw e;
		}
	}
	
	public String deserializeAsString(String json) throws Exception {
		try {
			GensonBuilder builder = new GensonBuilder();
			builder.setSkipNull(true); // if true, null value are not included into json
			Genson instance = builder.create();
			String result = instance.deserialize(json, String.class);
			return result;
		} catch(Exception e) {
			Utility.logger.error(e);
			throw e;
		}
	}
}
