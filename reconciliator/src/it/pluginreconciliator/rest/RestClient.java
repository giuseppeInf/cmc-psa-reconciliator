package it.pluginreconciliator.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import it.pluginreconciliator.utility.Utility;

public class RestClient {
	
	private static final String CONTENT_TYPE_JSON = "application/json";
	private static final String POST = "POST";
	private static final String GET = "GET";
	
	public String doPost(String service) throws Exception {
		return _call(POST, service, null, null);
	}
	
	public String doPost(String service, String json) throws Exception {
		return _call(POST, service, json, null);
	}
	
	public String doPost(String service, String json, String token) throws Exception {
		return _call(POST, service, json, token);
	}
	
	public String doGet(String service) throws Exception {
		return _call(GET, service, null, null);
	}
	
	public String doGet(String service, String json) throws Exception {
		return _call(GET, service, json, null);
	}
	
	public String doGet(String service, String json, String token) throws Exception {
		return _call(GET, service, json, token);
	}
		
	private String _call(String method, String service, String json, String token) throws Exception {
		HttpURLConnection conn = null;
		OutputStream os = null;
		BufferedReader br = null;
		try {
			
			URL url = new URL(service);
			conn = (HttpURLConnection)url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod(method);
			conn.setRequestProperty("Content-Type", CONTENT_TYPE_JSON);
			
			if(token != null) {
				conn.setRequestProperty("Authorization", "Bearer " + token);
			}
			
			if(json != null) {
				os = conn.getOutputStream();
				os.write(json.getBytes());
				os.flush();
			}
						
			if(conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new Exception("Http error code: " + conn.getResponseCode() + " - Http error message: " + conn.getResponseMessage());
			}
			
			br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String line;
			StringBuilder output = new StringBuilder();
			Utility.logger.info("Output from server.....");
			while((line = br.readLine()) != null) {
				output.append(line);
			}
			Utility.logger.info(output.toString());
			
			return output.toString();
			
		} catch(Exception e) {
			Utility.logger.error("Error while calling service " + service, e);
			throw e;
			
		} finally {
			if(br != null) {
				br.close();
			}
			if(os != null) {
				os.close();
			}
			if(conn != null) {
				conn.disconnect();
			}
		}
	}
}
