package it.pluginreconciliator.utility;

import java.io.File;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Logger;
import it.pluginreconciliator.commons.Constants;

public class Utility {

	public static final Logger logger = Logger.getLogger(Utility.class);

	public static boolean checkDocNumber(String docNumber) {
		if (docNumber == null)
			return false;

		if (docNumber.isEmpty())
			return false;

		if (docNumber.length() < 8)
			return false;

		return true;
	}

	public static boolean checkBrand(String brand) {
		if (brand == null)
			return false;

		if (brand.isEmpty())
			return false;

		if (!brand.equalsIgnoreCase(Constants.AC) && !brand.equalsIgnoreCase(Constants.AP)
				&& !brand.equalsIgnoreCase(Constants.DS))
			return false;

		return true;
	}

	public static boolean checkDealerCode(String dealerCode) {
		if (dealerCode == null)
			return false;

		if (dealerCode.isEmpty())
			return false;

		if (dealerCode.length() < 5)
			return false;

		return true;
	}

	public static Properties loadProperties() {
		Properties prop = new Properties();

		try {
		//	prop.load(Utility.class.getClassLoader().getResourceAsStream("config.properties"));
			prop.load(Utility.class.getClassLoader().getResourceAsStream("config_reconciliator.properties"));
		} catch (IOException ex) {
			ex.printStackTrace();  //"config" + File.separator + 
		}

		return prop;
	}
}
