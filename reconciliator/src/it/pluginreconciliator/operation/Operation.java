package it.pluginreconciliator.operation;


public class Operation {
	private int operationType;
	private String operationid; //id univoco dell'operazione settato dal backend (non valorizzare)
	private int client;
	
	public Operation() {}
	
	public Operation(int operationType, String operationid, int client) {
		this.operationType = operationType;
		this.operationid = operationid;
		this.client = client;
	}

	public int getOperationType() {
		return operationType;
	}

	public void setOperationType(int operationType) {
		this.operationType = operationType;
	}

	public String getOperationid() {
		return operationid;
	}

	public void setOperationid(String operationid) {
		this.operationid = operationid;
	}

	public int getClient() {
		return client;
	}

	public void setClient(int client) {
		this.client = client;
	}
}
