package it.pluginreconciliator.excelreader;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.JOptionPane;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import it.pluginreconciliator.bean.ReconciliatorBean;
import it.pluginreconciliator.utility.Utility;

public class ExcelReader {
	private String path;
	private ReconciliatorBean bean;
	private String docNumber;
	private String brand;
	private String dealerCode;

	public ExcelReader(String path) {
		this.path = path;
		/*
		 * this.docNumber = null; this.brand = null; this.dealerCode = null;
		 */
	}

	public Vector<ReconciliatorBean> readFile() {
		if (path == null || path.isEmpty()) {
			JOptionPane.showMessageDialog(null, "Non � stato indicato il path del file Excel..riprovare!");
			Utility.logger.error("Path file non indicato!");
			return null;
		}
		
		try {
			Vector<ReconciliatorBean> listOfBean = new Vector<ReconciliatorBean>();
			Workbook workbook = WorkbookFactory.create(new File(path));
			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();
			Iterator<Row> rowIterator = sheet.rowIterator();

			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				if (row.getRowNum() == 0)
					continue; // skip first row because it is the header

				// Now let's iterate over the columns of the current row
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();

					if (cell.getColumnIndex() == 1) { // DOCUMENT NUMBER
						docNumber = dataFormatter.formatCellValue(cell);
				      	Utility.logger.debug(docNumber);
						continue;
					}
					if (cell.getColumnIndex() == 5) { // BRAND
						brand = dataFormatter.formatCellValue(cell);
				    	Utility.logger.debug(brand);
						continue;
					}
					if (cell.getColumnIndex() == 6) { // DEALER CODE
						dealerCode = dataFormatter.formatCellValue(cell);
					 	Utility.logger.debug(dealerCode);
						break;
					}
				}
				if (Utility.checkDealerCode(dealerCode) && Utility.checkDocNumber(docNumber)
						&& Utility.checkBrand(brand)) {
					bean = new ReconciliatorBean(dealerCode, docNumber, brand);
					listOfBean.addElement(bean);
				}
			}
			return listOfBean;

		} catch (EncryptedDocumentException | InvalidFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null,
					"Impossibile accedere al file. Il file � utilizzato da un altro processo oppure non esiste!");
			Utility.logger.error(
					"Impossibile accedere al file. Il file � utilizzato da un altro processo oppure non esiste!");
			return null;
		}
				
//		return listOfBean;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public ReconciliatorBean getBean() {
		return bean;
	}

	public void setBean(ReconciliatorBean bean) {
		this.bean = bean;
	}
}
